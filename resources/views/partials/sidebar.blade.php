<aside id="sidebar-wrapper">
    <div class="sidebar-brand mt-1 mb-2">
        <a href="index.html">
            <img src="{!! asset('assets/images/logo/horizontal.png') !!}" class="navbar-brand-img h-50" alt="main_logo">
        </a>
    </div>

    {{-- <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">St</a>
    </div> --}}
    {{-- <hr class="mt-0"> --}}
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a class="nav-link" href="/dashboard"><i
                    class="fas fa-fire"></i><span>Dashboard</span></a></li>
        <li class="menu-header">Layanan</li>
        <li class="{{ Request::is('hosting*') ? 'active' : '' }}"><a class="nav-link" href="/hosting"><i
            class="fas fa-database"></i><span>Hosting</span></a></li>
        <li class="{{ Request::is('email*') ? 'active' : '' }}"><a class="nav-link" href="/email"><i
            class="fas fa-envelope"></i><span>Email</span></a></li>
        <li class="{{ Request::is('vpn*') ? 'active' : '' }}"><a class="nav-link" href="/vpn"><i
            class="fas fa-project-diagram"></i><span>VPN</span></a></li>
        <li class="{{ Request::is('hakakses*') ? 'active' : '' }}"><a class="nav-link" href="/hakakses"><i
            class="fas fa-dice-d20"></i><span>Hak Akses</span></a></li>
        <li class="{{ Request::is('ruangserver*') ? 'active' : '' }}"><a class="nav-link" href="/ruangserver"><i
            class="fas fa-server"></i><span>Ruang Server</span></a></li>
        
        @if (Auth::user()->role_id == 1)
            <li class="menu-header">Data Master</li>
            <li class="{{ Request::is('opd*') ? 'active' : '' }}"><a class="nav-link" href="/opd"><i
                class="fas fa-landmark"></i><span>OPD</span></a></li>
            <li class="{{ Request::is('manajemen-akses*') ? 'active' : '' }}"><a class="nav-link" href="/manajemen-akses"><i
                class="fas fa-tools"></i><span>Manajemen Hak Akses</span></a></li>
        @endif
    </ul>

    {{-- <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
        <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
            <i class="fas fa-rocket"></i> Documentation
        </a>
    </div> --}}
</aside>
