<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ruang Server</title>
</head>

<body>
    <div>
        <h4 align="center">Permohonan Izin Akses Pusat Data/Ruang Server</h4>
        <h4 align="center">Data Center/Server Room Access Request</h4>
        <h4>Informasi Pemohon/Applicant Informasi :</h4>
        <table border="1" cellpadding="2    ">
            <tbody>
                <tr>
                    <th width="30%">
                        Nama :
                        @foreach ($rs->timpemohon as $pem)
                            <br> {{ $loop->iteration }}. {{ $pem->nama }}
                        @endforeach
                    </th>
                    <th width="30%">
                        Perusahaan :
                        @foreach ($rs->timpemohon as $pem)
                                <br> {{ $loop->iteration }}. {{ $pem->perusahaan }}
                        @endforeach
                    </th>
                    <th width="40%">
                        Jabatan :
                        @foreach ($rs->timpemohon as $pem)
                               <br> {{ $loop->iteration }}.  {{ $pem->jabatan }}
                        @endforeach
                    </th>
                </tr>
                <tr>
                    <th>
                        Telpon Kantor :
                        <br>
                        {{ $rs->user->opd->telp }}
                    </th>
                    <th>
                        Nomor HP :
                        <br>
                        {{ $rs->user->telp }}                            
                    </th>
                    <th>
                        Alamat Email :
                        <br>
                        {{ $rs->user->opd->email }}
                    </th>
                </tr>
                <tr>
                    <th colspan="3">
                        Nomor KTP : {{ $rs->no_ktp }}
                    </th>
                </tr>
            </tbody>
        </table>
        <h4>Rincian Permohonan/Detail Request :</h4>
        <table border="1" cellpadding="2">
            <tbody>
                <tr>
                    <th>
                        Tanggal :
                        <br>
                        {{ $rs->tanggal }}
                    </th>
                    <th>
                        Waktu Datang :
                        <br>
                        {{ $rs->waktu_datang }}
                    </th>
                    <th>
                        Waktu Meninggalkan :
                        <br>
                        {{ $rs->waktu_meninggalkan }}
                    </th>
                </tr>
                <tr>
                    <th colspan="1">
                        Nomor Rak : 
                        <br>
                        {{ $rs->no_rak }}
                    </th>
                    <th colspan="2">
                        Aktivitas
                        <br>
                        {{ $rs->aktivitas == 'Pemasangan' ? '[X]' : '[ ]' }} Pemasangan   {{ $rs->aktivitas == 'Pembongkaran' ? '[X]' : '[ ]' }} Pembongkaran   {{ $rs->aktivitas == 'Setup' ? '[X]' : '[ ]' }} Setup
                        <br>
                        {{ $rs->aktivitas == 'Perawatan' ? '[X]' : '[ ]' }} Perawatan   {{ $rs->aktivitas == 'Perbaikan' ? '[X]' : '[ ]' }} Perbaikan
                    </th>
                </tr>
                <tr>
                    <th>
                        Nomor Kartu Akses :
                        <br>
                        {{ $rs->no_kartu_akses }}
                    </th>
                    <th>
                        Izin Pengambilan Gambar :
                        <br>
                        {{ $rs->izin_foto == 'Ya' ? '[X]' : '[ ]' }} Ya   {{ $rs->izin_foto == 'Tidak' ? '[X]' : '[ ]' }} Tidak
                    </th>
                    <th>
                        Catatan :
                        <div align="justify">{{ $rs->catatan }}</div>
                    </th>
                </tr>
            </tbody>
        </table>
        <h4>Regulasi  dan Peraturan Ruang Server/Data Center Rules and Regulations :</h4>
        <ul>
            <li align="justify">Pelanggan dan Pengunjung harus membawa identifikasi asli yang sesuai dengan mereka setiap kali mengajukan akses kedalam
                Data Center Pemerintah Kabupaten Sukoharjo. Memalsukan ata menyembunyikan identitas atau menolak untuk bekerja sama dengan
                petugas Data Center Pemerintah Kabupaten Sukoharjo merupakan pelanggaran terhadap kebijakan Data Center Pemerintah Kabupaten Sukoharjo.
            </li>
            <li align="justify">Segala jenis alas kaki tidak diizinkan masuk area lantai Data Center. Alas kaku harus dilepas di luar ruangan Data Center.
            </li>
            <li align="justify">Pelanggan dan Pengunjung dilarang meroko selama berada di dalam gedung atau area Data Center Pemerintah Kabupaten Sukoharjo.</li>
            <li align="justify">Segala jenis makanan dan minuman dilarang masuk ke dalam ruang Data Center Pemerintah Kabupaten Sukoharjo.</li>
            <li align="justify">Kamera tidak diizinkan di dalam Data Center Pemerintah Kabupaten Sukoharjo tanpa persetujuan terlebih dahulu. Hanya fot yang disetujui
                oleh petugas yang boleh disimpan.
            </li>
            <li align="justify">Semua Pelanggan dan Pengunjung dibawah pengawasan selama berada didalam area Data Center Pemerintah Kabupaten Sukoharjo.</li>
        </ul>
        <div align="justify">Saya telah membaca aturan dan regulasi untuk mengakses ruang server / Data Center Pemerintah Kabupaten Sukoharjo. Saya memahami aturan-aturan,
        regulasi, dan sanksi yang ada, dan saya setuju untuk mematuhi.</div>
    </div><br>
    <table>
        <tr>
            <th align="left">
                Mengetahui
                <br>
                Pengelola Data Center<br><br><br><br><br>

                ....................................
            </th>
            <th align="right">
                Sukoharjo, ....................................
                <br>
                <b>Pemohon <br><br><br><br><br></b>

                {{ $rs->user->nama }}
                
            </th>
        </tr>
    </table>
</body>

</html>
