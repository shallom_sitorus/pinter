@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/hosting" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Penambahan Spesifikasi Hosting</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/hosting">Hosting</a></div>
                <div class="breadcrumb-item">Penambahan Spesifikasi Hosting</div>
            </div>
        </div>

        <h2 class="section-title">Form Penambahan Spesifikasi Hosting</h2>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <form action="{{ url('/hosting/penambahan/store/' . $hosting->id) }}" method="post">
                        @csrf
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Instansi</h4>
                            </div>
                            <div class="card-body">
                                <input type="hidden" class="form-control" name="jenis_permohonan" value="Penambahan">
                                <input type="hidden" class="form-control" name="user" value="{{ Auth::user()->id }}">
                                <input type="hidden" class="form-control" name="no_hosting" value="{{ $no_hosting }}">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Kepala Instansi</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('nama_kepala') is-invalid @enderror"
                                            name="nama_kepala" value="{{ old('nama_kepala', $hosting->nama_kepala) }}" id="nama_kepala" required readonly>
                                        @error('nama_kepala')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP Kepala</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" class="form-control @error('nip_kepala') is-invalid @enderror"
                                            name="nip_kepala" value="{{ old('nip_kepala', $hosting->nip_kepala) }}" id="nip_kepala" required readonly>
                                        @error('nip_kepala')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-danger">
                            <div class="card-header">
                                <h4>Admin OPD</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                            name="nama" value="{{ old('nama', $hosting->developer->nama) }}" id="nama" required readonly>
                                        @error('nama')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('nip') is-invalid @enderror"
                                            name="nip" value="{{ old('nip', $hosting->developer->nip) }}" id="nip" readonly>
                                        @error('nip')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jabatan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('jabatan') is-invalid @enderror"
                                            name="jabatan" value="{{ old('jabatan', $hosting->developer->jabatan) }}" id="jabatan" required readonly>
                                        @error('jabatan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                                            name="email" value="{{ old('email', $hosting->developer->email) }}" id="email" required readonly>
                                        @error('email')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Telepon</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" class="form-control @error('phone') is-invalid @enderror"
                                            name="phone" value="{{ old('phone', $hosting->developer->phone) }}" id="phone" required readonly>
                                        @error('phone')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card card-info">
                            <div class="card-header">
                                <h4>Deskripsi Website</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Deskripsi Website</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('deskripsi_web') is-invalid @enderror" style="height: auto" name="deskripsi_web" id="deskripsi_web" required readonly>{{ old('deskripsi_web', $hosting->deskripsi_web) }}</textarea>
                                        @error('deskripsi_web')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Jenis Hosting</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="custom-select form-control @error('jenis_hosting') is-invalid @enderror" data-width="100%" name="jenis_hosting" id="selectOption" required>
                                            <option value="VPS"
                                            {{ $hosting->jenis_hosting == 'VPS' ? 'selected' : '' }}>
                                           VPS</option>
                                        <option value="Cpanel"
                                            {{ $hosting->jenis_hosting == 'Cpanel' ? 'selected' : '' }}>
                                            Cpanel</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="inputBox" style="display: none;">
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Operating System</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control @error('os') is-invalid @enderror"
                                                name="os" value="{{ old('os', $hosting->os) }}" id="os" required>
                                            @error('os')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Processor</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control @error('processor') is-invalid @enderror"
                                                name="processor" value="{{ old('processor', $hosting->processor) }}" id="processor" required>
                                            @error('processor')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">RAM</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control @error('ram') is-invalid @enderror"
                                                name="ram" value="{{ old('ram', $hosting->ram) }}" id="ram" required>
                                            @error('ram')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Storage</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('storage') is-invalid @enderror"
                                            name="storage" value="{{ old('storage', $hosting->storage) }}" id="storage" required>
                                        @error('storage')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Subdomain</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('subdomain_baru') is-invalid @enderror"
                                            name="subdomain_baru" value="{{ old('subdomain_baru', $hosting->subdomain_baru) }}" id="subdomain_baru" required readonly>
                                        @error('subdomain_baru')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <input type="hidden" name="status" value="1">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary" id="btn-simpan">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script>
        var selectOption = document.getElementById('selectOption');
        var inputBox = document.getElementById('inputBox');
    
        selectOption.addEventListener('select', function() {
            if (this.value == 'VPS') {
                inputBox.style.display = 'block';
            } else {
                inputBox.style.display = 'none';
            }
        });
    
        if (selectOption.value == 'VPS') {
            inputBox.style.display = 'block';
        }
    </script>

    <script>
        var select = document.getElementById('selectOption');
        var inputBox = document.getElementById('inputBox');
        var os = document.getElementById('os');
        var processor = document.getElementById('processor');
        var ram = document.getElementById('ram');

        document.getElementById('selectOption').addEventListener('change', function() {
            var selectedOption = this.options[this.selectedIndex].value;
            
            if (selectedOption === 'VPS') {
                inputBox.style.display = 'block';
                os.setAttribute('required', '');
                processor.setAttribute('required', '');
                ram.setAttribute('required', '');
            } else {
                inputBox.style.display = 'none';
                os.removeAttribute('required');
                processor.removeAttribute('required');
                ram.removeAttribute('required');
            }
        }); 

        if (select.value === 'VPS') {
            inputDiv.style.display = 'block';
            os.setAttribute('required', '');
            processor.setAttribute('required', '');
            ram.setAttribute('required', '');
        } else {
            inputBox.style.display = 'none';
            os.removeAttribute('required');
            processor.removeAttribute('required');
            ram.removeAttribute('required');
        }
    </script>
@endsection