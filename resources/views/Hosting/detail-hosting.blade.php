@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/hosting" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Detail Layanan Hosting</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/hosting">Hosting</a></div>
                <div class="breadcrumb-item">Detail Layanan Hosting</div>
            </div>
        </div>

        <h2 class="section-title">Detail Layanan Hosting</h2>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    @php
                        if ($hosting->jenis_permohonan == 'Baru') {
                            $jp = 'Pembuatan Baru';
                        } elseif ($hosting->jenis_permohonan == 'Perubahan') {
                            $jp = 'Perubahan Subdomain';
                        } elseif ($hosting->jenis_permohonan == 'Penambahan') {
                            $jp = 'Penambahan Spesifikasi';
                        }
                        
                        if ($hosting->status == 1) {
                            $warna = 'warning';
                        } elseif ($hosting->status == 2) {
                            $warna = 'primary';
                        } elseif ($hosting->status == 3) {
                            $warna = 'success';
                        } else {
                            $warna = 'danger';
                        }
                    @endphp
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Instansi</h4>
                        </div>
                        <div class="card-body">
                            <form action="" method="post">
                                @csrf
                                @php
                                if ($hosting->jenis_permohonan == 'baru') {
                                    $jp = 'Pembuatan Baru';
                                } elseif ($hosting->jenis_permohonan == 'perubahan') {
                                    $jp = 'Perubahan Subdomain';
                                } elseif ($hosting->jenis_permohonan == 'penambahan') {
                                    $jp = 'Penambahan Spesifikasi';
                                }
                                
                                if ($hosting->status == 1) {
                                    $warna = 'warning';
                                } elseif ($hosting->status == 2) {
                                    $warna = 'primary';
                                } elseif ($hosting->status == 3) {
                                    $warna = 'success';
                                } else {
                                    $warna = 'danger';
                                }
                            @endphp
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis
                                    Permohonan</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text"
                                        class="form-control @error('jenis_permohonan') is-invalid @enderror"
                                        name="jenis_permohonan" value="{{ $jp }}" id="jenis_permohonan" required
                                        readonly>
                                    @error('jenis_permohonan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Kepala
                                    Instansi</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('nama_kepala') is-invalid @enderror"
                                        name="nama_kepala" value="{{ old('nama_kepala', $hosting->nama_kepala) }}"
                                        id="nama_kepala" required readonly>
                                    @error('nama_kepala')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP Kepala</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="number" class="form-control @error('nip_kepala') is-invalid @enderror"
                                        name="nip_kepala" value="{{ old('nip_kepala', $hosting->nip_kepala) }}"
                                        id="nip_kepala" required readonly>
                                    @error('nip_kepala')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-danger">
                        <div class="card-header">
                            <h4>Admin OPD</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                        name="nama" value="{{ old('nama', $hosting->developer->nama) }}" id="nama"
                                        required readonly>
                                    @error('nama')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('nip') is-invalid @enderror"
                                        name="nip" value="{{ old('nip', $hosting->developer->nip) }}" id="nip"
                                        required readonly>
                                    @error('nip')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jabatan</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('jabatan') is-invalid @enderror"
                                        name="jabatan" value="{{ old('jabatan', $hosting->developer->jabatan) }}"
                                        id="jabatan" required readonly>
                                    @error('jabatan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email', $hosting->developer->email) }}"
                                        id="email" required readonly>
                                    @error('email')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Telepon</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="number" class="form-control @error('phone') is-invalid @enderror"
                                        name="phone" value="{{ old('phone', $hosting->developer->phone) }}"
                                        id="phone" required readonly>
                                    @error('phone')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card card-info">
                        <div class="card-header">
                            <h4>Deskripsi Website</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Deskripsi
                                    Website</label>
                                <div class="col-sm-12 col-md-7">
                                    <textarea class="form-control @error('deskripsi_web') is-invalid @enderror" style="height: auto" name="deskripsi_web" id="deskripsi_web"
                                        required readonly>{{ old('deskripsi_web', $hosting->deskripsi_web) }}</textarea>
                                    @error('deskripsi_web')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jenis Hosting</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text"
                                        class="form-control @error('jenis_hosting') is-invalid @enderror"
                                        name="jenis_hosting" value="{{ old('jenis_hosting', $hosting->jenis_hosting) }}"
                                        id="jenis_hosting" required readonly>
                                    @error('jenis_hosting')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            @if ($hosting->jenis_hosting == 'VPS')
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Operating
                                        System</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('os') is-invalid @enderror"
                                            name="os" value="{{ old('os', $hosting->os) }}" id="os" required
                                            readonly>
                                        @error('os')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Processor</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text"
                                            class="form-control @error('processor') is-invalid @enderror"
                                            name="processor" value="{{ old('processor', $hosting->processor) }}"
                                            id="processor" required readonly>
                                        @error('processor')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">RAM</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('ram') is-invalid @enderror"
                                            name="ram" value="{{ old('ram', $hosting->ram) }}" id="ram"
                                            required readonly>
                                        @error('ram')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Storage</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control @error('storage') is-invalid @enderror"
                                        name="storage" value="{{ old('storage', $hosting->storage) }}" id="storage"
                                        required readonly>
                                    @error('storage')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Subdomain</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text"
                                        class="form-control @error('subdomain_baru') is-invalid @enderror"
                                        name="subdomain_baru"
                                        value="{{ old('subdomain_baru', $hosting->subdomain_baru) }}" id="subdomain_baru"
                                        readonly required>
                                    @error('subdomain_baru')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            @if ($hosting->subdomain_lama != null)
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Subdomain
                                        Lama</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text"
                                            class="form-control @error('subdomain_lama') is-invalid @enderror"
                                            name="subdomain_lama"
                                            value="{{ old('subdomain_lama', $hosting->subdomain_lama) }}"
                                            id="subdomain_lama" required readonly>
                                        @error('subdomain_lama')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status</label>
                                <div class="col-sm-12 col-md-7">
                                    <span
                                        class="badge badge-{{ $warna }}">{{ $hosting->statushosting->status }}</span>
                                </div>
                            </div>
                            @if ($hosting->status == 4)
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alasan
                                        Ditolak</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" id="keterangan" readonly
                                            required>{{ old('keterangan', $hosting->keterangan) }}</textarea>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
