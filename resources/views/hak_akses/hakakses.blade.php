@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Hak Akses</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item">Hak Akses</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Daftar Permohonan Hak Akses</h2>
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="row">
                                <form action="{{ url('/hakakses') }}" method="POST" class="col-md-4 col-12" id="myForm"
                                    role="form">
                                    @method('get')
                                    @csrf
                                    <div class="form-group">
                                        <label>Tahun</label>
                                        <select class="form-control" name="tahun" id="mySelect">
                                            @foreach ($tahun as $thn)
                                                <option value="{{ $thn }}"
                                                    @if (request()->tahun) {{ request()->tahun == $thn ? 'selected' : '' }}
                                                    @else {{ date('Y') == $thn ? 'selected' : '' }} @endif>
                                                    {{ $thn }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </form>
                                <div class="form-group col-md-4 col-12">
                                    <label>Jenis Akses</label>
                                    <select class="form-control selectric" id="jenis_filter">
                                        <option value="">Semua</option>
                                        @foreach ($jenisakses as $jen)
                                            <option value="{{ $jen->nama }}">{{ $jen->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-12">
                                    <label>Status</label>
                                    <select class="form-control selectric" id="status_filter">
                                        <option value="">Semua</option>
                                        @foreach ($status as $st)
                                            <option value="{{ $st->id }}">{{ $st->status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Hak Akses</h4>
                            @if (Auth::user()->role_id == 2)
                                <div class="card-header-action">
                                    <a href="/hakakses/add" class="btn btn-primary btn-icon icon-right"><i
                                            class="fas fa-plus"></i>
                                        Ajukan Permohonan</a>
                                </div>
                            @endif
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="{{ Auth::user()->role_id == 1 ? 'tableAkses' : 'tableAkses-2' }}" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center align-middle">
                                                #
                                            </th>
                                            @if (Auth::user()->role_id == 1)
                                                <th class="text-center align-middle">OPD</th>
                                            @endif
                                            <th class="text-center align-middle">Nomor Hak Akses</th>
                                            <th class="text-center align-middle">Tujuan/Jenis Akses</th>
                                            <th class="text-center align-middle">Waktu</th>
                                            <th class="text-center align-middle">Tanggal Awal</th>
                                            @if (Auth::user()->role_id == 2)
                                                <th class="text-center align-middle">Cetak Permohonan</th>
                                            @endif
                                            <th class="text-center align-middle">Formulir Permohonan</th>
                                            <th class="text-center align-middle" width="15%">Status</th>
                                            <th class="align-middle">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($hakakses as $hak)
                                            @php
                                                if ($hak->status == 1) {
                                                    $warna = 'warning';
                                                } elseif ($hak->status == 2) {
                                                    $warna = 'primary';
                                                } elseif ($hak->status == 3) {
                                                    $warna = 'success';
                                                } else {
                                                    $warna = 'danger';
                                                }
                                            @endphp
                                            <tr>
                                                <td class="text-center">
                                                    {{ $loop->iteration }}
                                                </td>
                                                @if (Auth::user()->role_id == 1)
                                                    <td>{{ $hak->user->opd->nama }}</td>
                                                @endif
                                                <td class="text-center">{{ $hak->no_hak_akses }}</td>
                                                <td class="text-center">{{ $hak->jenisakses->nama }}</td>
                                                <td class="text-center">{{ $hak->waktu }}</td>
                                                <td class="text-center">{{ date('d M Y', strtotime($hak->tanggal_awal)) }} - {{ date('d M Y', strtotime($hak->tanggal_akhir)) }}</td>
                                                @if (Auth::user()->role_id == 2)
                                                    <td class="text-center">
                                                        <a href="/hakakses/cetak/{{ $hak->id }}"
                                                            class="btn btn-outline-success btn-icon btn-sm" target="blank">
                                                            <i class="fas fa-download"></i> Download
                                                        </a>
                                                    </td>
                                                @endif
                                                <td class="text-center">
                                                    @if ($hak->surat)
                                                        <a href="{{ asset('form_akses/' . $hak->surat) }}" target="blank"
                                                            class="mr-1"><svg xmlns="http://www.w3.org/2000/svg"
                                                                width="24" fill="red" viewBox="0 0 384 512">
                                                                <path
                                                                    d="M64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V160H256c-17.7 0-32-14.3-32-32V0H64zM256 0V128H384L256 0zM64 224H88c30.9 0 56 25.1 56 56s-25.1 56-56 56H80v32c0 8.8-7.2 16-16 16s-16-7.2-16-16V320 240c0-8.8 7.2-16 16-16zm24 80c13.3 0 24-10.7 24-24s-10.7-24-24-24H80v48h8zm72-64c0-8.8 7.2-16 16-16h24c26.5 0 48 21.5 48 48v64c0 26.5-21.5 48-48 48H176c-8.8 0-16-7.2-16-16V240zm32 112h8c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16h-8v96zm96-128h48c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v32h32c8.8 0 16 7.2 16 16s-7.2 16-16 16H304v48c0 8.8-7.2 16-16 16s-16-7.2-16-16V304 240c0-8.8 7.2-16 16-16z" />
                                                            </svg></a>
                                                        @if ($hak->status == 1 && Auth::user()->role_id == 2)
                                                            <div class="table-links">
                                                                <div class="bullet"></div>
                                                                <a href="#" data-toggle="modal"
                                                                    data-target="#uploadForm{{ $hak->id }}">Edit</a>
                                                            </div>
                                                        @endif
                                                    @else
                                                        @if (Auth::user()->role_id == 2)
                                                            <button data-toggle="modal"
                                                                data-target="#uploadForm{{ $hak->id }}"
                                                                class="btn btn-outline-primary btn-icon btn-sm">
                                                                <i class="fas fa-upload"></i> Upload
                                                            </button>
                                                        @else
                                                            -
                                                        @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="float-center dropdown">
                                                        <div class="badge badge-{{ $warna }} {{ Auth::user()->role_id == 1 ? 'dropdown-toggle' : '' }}"
                                                            type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                            {{ $hak->statusakses->status }} <span
                                                                class="d-none">{{ $hak->status }}</span></div>
                                                        @if (Auth::user()->role_id == 1)
                                                            <div class="dropdown-menu" x-placement="bottom-start"
                                                                style="position: absolute; transform: translate3d(0px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                @foreach ($status as $st)
                                                                    @if ($st->status != $hak->statusakses->status)
                                                                        @if ($st->status != 'Ditolak')
                                                                            <form
                                                                                action="/hakakses/status/{{ $hak->id }}"
                                                                                method="post">@csrf @method('put')
                                                                                <input type="hidden"
                                                                                    value="{{ $st->id }}"
                                                                                    name="status"><button type="submit"
                                                                                    class="dropdown-item mb-1 btn">{{ $st->status }}</button>
                                                                            </form>
                                                                        @else
                                                                            <button data-toggle="modal"
                                                                                data-target="#status{{ $hak->id }}"
                                                                                class="dropdown-item btn">{{ $st->status }}</button>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    </div>
                                                    @if ($hak->status == 4)
                                                        <div class="table-links pt-1">
                                                            <div class="bullet"></div>
                                                            <a href="#" data-toggle="modal"
                                                                data-target="#lihatStatus{{ $hak->id }}">Lihat
                                                                detail</a>
                                                        </div>
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="float-center dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a href="" data-toggle="modal"
                                                                data-target="#detail{{ $hak->id }}"
                                                                class="dropdown-item has-icon"><i class="fas fa-eye"></i>
                                                                Detail</a>
                                                            @if ($hak->status == 1 && Auth::user()->role_id == 2)
                                                                <a href="/hakakses/edit/{{ $hak->id }}"
                                                                    class="dropdown-item has-icon"><i
                                                                        class="fas fa-pencil-alt"></i>
                                                                    Edit</a>
                                                            @endif
                                                            @if (Auth::user()->role_id == 1)
                                                            <a href="/hakakses/cetak/{{ $hak->id }}"
                                                                class="dropdown-item has-icon" target="blank"><i
                                                                    class="fas fa-print"></i>
                                                                Cetak</a>
                                                            @endif
                                                            @if (Auth::user()->role_id == 1)
                                                                <div class="dropdown-divider"></div>
                                                                <form class="btn-delete"
                                                                    action="/hakakses/delete/{{ $hak->id }}"
                                                                    method="post">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <a type="submit"
                                                                        class="dropdown-item has-icon text-danger">
                                                                        <i class="fas fa-trash-alt"></i> Delete
                                                                    </a>
                                                                </form>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @foreach ($hakakses as $hak)
        <!-- Modal Upload Formulir -->
        <div class="modal fade" tabindex="-1" role="dialog" id="uploadForm{{ $hak->id }}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload Formulir Permohonan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Silakan upload formulir permohonan yang telah dicetak dan ditandatangani.</p>
                        <form action="{{ url('/hakakses/upload/' . $hak->id) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="custom-file">
                                <input type="file" class="custom-file-input @error('surat') is-invalid @enderror"
                                    id="customFile" name="surat" accept=".pdf" required>
                                <label class="custom-file-label"
                                    for="customFile">{{ $hak->surat ?? 'Choose file' }}</label>
                            </div>
                            <input type="hidden" name="oldFile" value="{{ $hak->surat }}">
                            @error('surat')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="submit" class="btn btn-primary btn-shadow" id="">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal Status Ditolak -->
        <div class="modal fade" tabindex="-1" role="dialog" id="status{{ $hak->id }}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Alasan Penolakan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Tulis alasan menolak permohonan email.</p>
                        <form action="{{ url('/hakakses/status/' . $hak->id) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <textarea class="form-control @error('keterangan') is-invalid @enderror" name="keterangan" id="keterangan" required>{{ old('keterangan', $hak->keterangan) }}</textarea>
                            <input type="hidden" name="status" value="4">
                            @error('keterangan')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                    </div>
                    <div class="modal-footer bg-whitesmoke">
                        <button type="submit" class="btn btn-primary btn-shadow" id="">Simpan</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Modal Lihat Status -->
        <div class="modal fade" tabindex="-1" role="dialog" id="lihatStatus{{ $hak->id }}">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-whitesmoke">
                    <div class="modal-header">
                        <h5 class="modal-title">Detail Alasan Penolakan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="activities">
                                    <div class="activity">
                                        <div class="activity-icon bg-danger text-white shadow-danger">
                                            <i class="fas fa-exclamation-triangle"></i>
                                        </div>
                                        <div class="activity-detail">
                                            <p>{{ $hak->keterangan }}. Silakan ajukan permohonan ulang.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Detail -->
        @include('hak_akses.detail-hakakses')
    @endforeach
@endsection
