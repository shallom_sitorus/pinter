<div class="modal fade" tabindex="-1" role="dialog" id="detail{{ $hak->id }}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Permintaan Hak Akses</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Lihat detail permintaan hak akses.</p>
                @php
                    if ($hak->status == 1) {
                        $warna = 'warning';
                    } elseif ($hak->status == 2) {
                        $warna = 'primary';
                    } elseif ($hak->status == 3) {
                        $warna = 'success';
                    } else {
                        $warna = 'danger';
                    }
                @endphp
                <h6 class="text-primary">
                    <div class="bullet"></div> Penanggungjawab OPD
                </h6>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-sm">
                        <tr>
                            <th width="25%" class="pl-3" style="vertical-align: middle; background: whitesmoke">
                                Nama OPD</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->user->opd->nama }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Nama
                            </th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->user->nama }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">NIP</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->user->nip }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Telepon</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->user->opd->telp }}" disabled></td>
                        </tr>
                    </table>
                </div>
                <h6 class="text-primary">
                    <div class="bullet"></div> Penanggungjawab Pihak Ke-III
                </h6>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-sm">
                        <tr>
                            <th width="25%" class="pl-3" style="vertical-align: middle; background: whitesmoke">
                                Nama Perusahaan</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->pihakketiga->nama_perusahaan }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Nama
                            </th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->pihakketiga->nama }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Telepon</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->pihakketiga->telp }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Jabatan di
                                Perusahaan</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->pihakketiga->jabatan }}" disabled></td>
                        </tr>
                    </table>
                </div>
                <h6 class="text-primary">
                    <div class="bullet"></div> Detail Hak Akses
                </h6>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-sm">
                        <tr>
                            <th width="25%" class="pl-3" style="vertical-align: middle; background: whitesmoke">
                                Tujuan/Jenis Akses</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->jenisakses->nama }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Alasan
                            </th>
                            <td>
                                <textarea class="form-control bg-transparent border-0" id="alasan" style="height: auto" disabled>{{ $hak->alasan }}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Waktu</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $hak->waktu }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Masa Berlaku</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ date('d M Y', strtotime($hak->tanggal_awal)) }} - {{ date('d M Y', strtotime($hak->tanggal_akhir)) }}"
                                    disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Status</th>
                            <td class="pl-3"><span
                                    class="badge badge-{{ $warna }}">{{ $hak->statusakses->status }}</span>
                            </td>
                        </tr>
                        @if ($hak->status == 4)
                            <tr>
                                <th class="pl-3" style="vertical-align: middle; background: whitesmoke">
                                    Alasan/Keterangan</th>
                                <td class="pl-3">
                                    <p class="text-danger mt-3">{{ $hak->keterangan }}</p>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke">
                <button type="submit" class="btn btn-secondary btn-shadow" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
