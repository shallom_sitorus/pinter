<div class="modal fade" tabindex="-1" role="dialog" id="detail{{ $vpn->id }}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Permohonan VPN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Lihat detail permohonan VPN.</p>
                <div class="table-responsive">
                    @php
                        if ($vpn->status == 1) {
                            $warna = 'warning';
                        } elseif ($vpn->status == 2) {
                            $warna = 'primary';
                        } elseif ($vpn->status == 3) {
                            $warna = 'success';
                        } else {
                            $warna = 'danger';
                        }
                    @endphp
                    <table class="table table-striped table-bordered table-sm">
                        <tr>
                            <th width="25%" class="pl-3" style="vertical-align: middle; background: whitesmoke">
                                Instansi</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $vpn->user->opd->nama }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Jenis Permohonan
                            </th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $vpn->jenis_permohonan }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Keperluan</th>
                            <td><textarea class="form-control bg-transparent border-0" style="height: auto" disabled>{{ $vpn->keperluan }}</textarea></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Rentang waktu</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ date('d M Y', strtotime($vpn->tgl_awal)) }} - {{ date('d M Y', strtotime($vpn->tgl_berakhir)) }}" disabled></td>
                        </tr>
                        @if ($vpn->jenis_permohonan == 'Ganti Password')
                            <tr>
                                <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Permasalahan</th>
                                <td><textarea class="form-control bg-transparent border-0" style="height: auto" disabled>{{ $vpn->permasalahan }}</textarea></td>
                            </tr>
                            <tr>
                                <th class="pl-3" style="vertical-align: middle; background: whitesmoke">IP VPN</th>
                                <td><input class="form-control bg-transparent border-0" type="text"
                                        value="{{ $vpn->ip_vpn }}" disabled></td>
                            </tr>
                        @endif
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Password</th>
                            <td>
                                <div class="input-group">
                                    <input type="password" id="password{{ $vpn->id }}"
                                        class="form-control bg-transparent border-0" name="password"
                                        value="{{ $vpn->password }}" disabled>
                                    <div class="input-group-append">
                                        <span id="mybutton{{ $vpn->id }}"
                                            onclick="showPassword2('password{{ $vpn->id }}', 'mybutton{{ $vpn->id }}')"
                                            class="input-group-text bg-transparent border-0">
                                            <i class="fas fa-eye"></i>
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Status</th>
                            <td class="pl-3"><span class="badge badge-{{ $warna }}">{{ $vpn->statusvpn->status }}</span></td>
                        </tr>
                        @if ($vpn->status == 4)
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Alasan/Keterangan</th>
                            <td class="pl-3"><p class="text-danger mt-3">{{ $vpn->keterangan }}</p></td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke">
                <button type="submit" class="btn btn-secondary btn-shadow" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
