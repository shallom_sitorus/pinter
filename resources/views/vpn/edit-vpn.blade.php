@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/vpn" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Edit Permohonan VPN</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/vpn">VPN</a></div>
                <div class="breadcrumb-item">Edit Permohonan VPN</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Form Edit Permohonan VPN</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ url('vpn/update/'. $vpn->id) }}" method="post">
                                @method('put')
                                @csrf
                                @if ($vpn->jenis_permohonan == 'Baru')
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Keperluan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('keperluan') is-invalid @enderror" style="height: auto" name="keperluan" id="keperluan" required>{{ old('keperluan', $vpn->keperluan) }}</textarea>
                                        @error('keperluan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Rentang Waktu</label>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="text"
                                            class="form-control datepicker @error('tgl_awal') is-invalid @enderror"
                                            name="tgl_awal" value="{{ old('tgl_awal', $vpn->tgl_awal) }}" id="tgl_awal"
                                            required>
                                        @error('tgl_awal')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-sm-12 col-md-1 d-flex justify-content-center align-items-center"><i class="fas fa-minus"></i></div>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="text"
                                            class="form-control datepicker @error('tgl_berakhir') is-invalid @enderror"
                                            name="tgl_berakhir" value="{{ old('tgl_berakhir', $vpn->tgl_berakhir) }}" id="tgl_berakhir"
                                            required>
                                        @error('tgl_berakhir')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                @endif
                                @if ($vpn->jenis_permohonan == 'Ganti Password')
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Permasalahan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('permasalahan') is-invalid @enderror" style="height: auto" name="permasalahan" id="permasalahan" required>{{ old('permasalahan', $vpn->permasalahan) }}</textarea>
                                        @error('permasalahan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">IP VPN</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text"
                                            class="form-control @error('ip_vpn') is-invalid @enderror"
                                            name="ip_vpn" value="{{ old('ip_vpn', $vpn->ip_vpn) }}" id="ip_vpn"
                                            required>
                                        @error('ip_vpn')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Password</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="input-group">
                                            <input type="password" id="password"
                                                class="form-control @error('password') is-invalid @enderror" name="password"
                                                value="{{ old('password', $vpn->password) }}" required>
                                            <div class="input-group-append">
                                                <span id="mybutton" onclick="showPassword()" class="input-group-text">
                                                    <i class="fas fa-eye"></i>
                                                </span>
                                            </div>
                                        </div>
                                        @error('password')
                                            <div class="invalid-feedback d-block">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>  
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
