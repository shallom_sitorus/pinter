<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>PINTER | Pelayanan Admnistrasi Sistem Elektronik</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="icon" type="image/png" href="{!! asset('img/kabsukoharjo.png') !!}">
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{!! asset('node_modules/bootstrap-social/bootstrap-social.css') !!}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{!! asset('css/style.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/components.css') !!}">
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="d-flex flex-wrap align-items-stretch">
                <div class="col-lg-5 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
                    <div class="p-4 m-3 mt-5">
                        <div class="d-flex justify-content-center">
                            <img src="{{ asset('img/kabsukoharjo.png') }}" alt="logo" width="40"
                                class="mb-5 mt-2">
                            <img src="{{ asset('img/kominfo.png') }}" alt="logo" width="55"
                                class="mb-5 mx-2 mt-2">
                            <img src="{{ asset('assets/images/logo/rectangle.png') }}" alt="logo" width="50"
                                class="mb-5 mt-2">
                        </div>
                        <h4 class="text-dark text-center font-weight-normal"><span class="font-weight-bold">LOGIN</span>
                        </h4>
                        <p class="text-muted text-center">Masukkan NIP dan kata sandi Anda untuk dapat login.</p>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="nip">NIP</label>
                                <input type="number" class="form-control @error('nip') is-invalid @enderror"
                                    name="nip" value="{{ old('nip') }}" tabindex="1" required autofocus>
                                @error('nip')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="d-block">
                                    <label for="password" class="control-label">Password</label>
                                </div>
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                    name="password" value="{{ old('password') }}" required autofocus tabindex="2">
                                @error('password')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                                {!! NoCaptcha::display() !!}
                                {!! NoCaptcha::renderJs() !!}
                                @error('g-recaptcha-response')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            <div class="form-group text-center mt-4">
                                <button type="submit" class="btn btn-primary btn-lg btn-icon" tabindex="4">
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-7 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom"
                    data-background="{{ asset('img/bg_login.png') }}">
                    <div class="absolute-bottom-left index-2">
                        <div class="text-light p-5 pb-2">
                            <div class="mb-5 pb-3">
                                <h1 class="mb-2 display-4 font-weight-bold">PINTER</h1>
                                <h5 class="font-weight-normal text-muted-transparent">Pelayanan Administrasi Sistem
                                    Elektronik</h5>
                            </div>
                            Dinas Komunikasi dan Informatika Kabupaten Sukoharjo
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script src='https://www.google.com/recaptcha/api.js'></script>
    {{-- <script src='https://www.google.com/recaptcha/'></script> --}}
    {{-- <script src='https://www.gstatic.com/recaptcha/'></script> --}}

    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{!! asset('js/stisla.js') !!}"></script>


    <!-- JS Libraies -->

    <!-- Template JS File -->
    <script src="{!! asset('js/scripts.js') !!}"></script>
    <script src="{!! asset('js/custom.js') !!}"></script>

    <!-- Page Specific JS File -->
</body>

</html>
