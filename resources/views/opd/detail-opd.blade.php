@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Detail OPD</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/opd">OPD</a></div>
                <div class="breadcrumb-item">Detail OPD</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title">Detail OPD</h2>
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card author-box card-primary">
                        <div class="card-body">
                            <div class="author-box-left">
                                <img alt="image" src="{!! asset('img/kabsukoharjo.png') !!}" class="author-box-picture">
                                <div class="clearfix"></div>
                            </div>
                            <div class="author-box-details">
                                <div class="author-box-name">
                                    <h5 class="font-weight-bold text-primary">{{ $opd->nama }}</h5>
                                </div>
                                <div class="author-box-job" style="font-size: 13px">{{ $opd->email }}</div>
                                <div class="author-box-description" style="font-size: 14px">
                                    <p>{{ $opd->alamat }} | {{ $opd->telp }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="section-title">User</h2>
                    <div class="card card-info">
                        <div class="card-header">
                            <h4>Users/Admin OPD</h4>
                            <div class="card-header-action">
                                <a href="/opd/{{ $opd->id }}/user/add" class="btn btn-primary btn-icon icon-right"><i class="fas fa-plus"></i>
                                    Tambah</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center">NIP</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Telepon</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($listuser as $user)
                                            <tr>
                                                <td class="text-center align-middle">
                                                    {{ $loop->iteration }}
                                                </td>
                                                <td class="text-center align-middle">
                                                    @if ($user->foto)
                                                        <img alt="image" src="{{ asset('img/fotousers/' . $user->foto) }}"
                                                            class="rounded-circle mr-3 bg-cover" width="35" data-toggle="tooltip"
                                                            title="{{ $user->nama }}">
                                                    @else
                                                        <img alt="image" src="{{ asset('img/avatar/avatar-3.png') }}"
                                                            class="rounded-circle mr-3" width="35" data-toggle="tooltip"
                                                            title="{{ $user->nama }}">
                                                    @endif
                                                    {{ $user->nama }}
                                                </td>
                                                <td class="text-center align-middle">{{ $user->nip }}</td>
                                                <td class="text-center align-middle">{{ $user->email }}</td>
                                                <td class="text-center align-middle">{{ $user->telp }}</td>
                                                <td class="align-middle">
                                                    <div class="float-center dropdown">
                                                        <a href="#" data-toggle="dropdown"><i
                                                                class="fas fa-ellipsis-h"></i></a>
                                                        <div class="dropdown-menu">
                                                            <div class="dropdown-title">Options</div>
                                                            <a data-toggle="modal" data-target="#detailUser{{ $user->id }}"
                                                                class="dropdown-item has-icon"><i class="fas fa-eye"></i>
                                                                Detail</a>
                                                            <a href="/opd/{{ $opd->id }}/user/edit/{{ $user->id }}"
                                                                class="dropdown-item has-icon"><i class="fas fa-pencil-alt"></i>
                                                                Edit</a>
                                                            <div class="dropdown-divider"></div>
                                                            <form class="btn-delete"
                                                                action="/opd/user/delete/{{ $user->id }}" method="post">
                                                                @method('delete')
                                                                @csrf
                                                                <a type="submit"
                                                                    class="dropdown-item has-icon text-danger">
                                                                    <i class="fas fa-trash-alt"></i> Delete
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('user.detail-user')
@endsection
