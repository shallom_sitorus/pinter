@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/opd" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Edit OPD</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/opd">OPD</a></div>
                <div class="breadcrumb-item">Edit OPD</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Form Edit OPD</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ url('opd/update/'. $opd->id) }}" method="post">
                                @csrf
                                @method('put')
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama OPD</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                            name="nama" value="{{ old('nama', $opd->nama) }}" id="nama" required>
                                        @error('nama')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Email</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                                            name="email" value="{{ old('email', $opd->email) }}" id="email" required>
                                        @error('email')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Telepon</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="tel" class="form-control @error('telp') is-invalid @enderror"
                                            name="telp" value="{{ old('telp', $opd->telp) }}" id="telp" required>
                                        @error('telp')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Alamat</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('alamat') is-invalid @enderror" style="height: 100px" name="alamat" id="alamat" required>{{ old('alamat', $opd->alamat) }}</textarea>
                                        @error('alamat')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
