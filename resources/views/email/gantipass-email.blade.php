@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/email" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Permohonan Ganti Password Email</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/email">Email</a></div>
                <div class="breadcrumb-item">Permohonan Ganti Password Email</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Form Permohonan Ganti Password Email</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ url('email/create-gantipass/'. $email->id) }}" method="post">
                                @csrf
                                <input type="hidden" class="form-control" name="no_email" value="{{ $no_email }}">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Email</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="input-group">
                                            <input type="text"
                                                class="form-control" id="email" name="nama_email" value="{{ $email->nama_email }}" readonly>
                                            <div class="input-group-append">
                                                <div class="input-group-text">@sukoharjokab.go.id</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Password</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="input-group">
                                            <input type="password" id="password"
                                                class="form-control @error('password') is-invalid @enderror" name="password"
                                                value="{{ old('password') }}" required>
                                            <div class="input-group-append">
                                                <span id="mybutton" onclick="showPassword()" class="input-group-text">
                                                    <i class="fas fa-eye"></i>
                                                </span>
                                            </div>
                                        </div>
                                        @error('password')
                                            <div class="invalid-feedback d-block">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="jenis_permohonan" value="Ganti Password">
                                <input type="hidden" name="nama_pengguna" value="{{ $email->nama_pengguna }}">
                                <input type="hidden" name="status" value="1">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary" id="btn-simpan">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
