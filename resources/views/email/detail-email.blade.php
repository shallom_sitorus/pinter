<div class="modal fade" tabindex="-1" role="dialog" id="detail{{ $email->id }}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Permohonan Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Lihat detail permohonan email.</p>
                <div class="table-responsive">
                    @php
                        if ($email->status == 1) {
                            $warna = 'warning';
                        } elseif ($email->status == 2) {
                            $warna = 'primary';
                        } elseif ($email->status == 3) {
                            $warna = 'success';
                        } else {
                            $warna = 'danger';
                        }
                    @endphp
                    <table class="table table-striped table-bordered table-sm">
                        <tr>
                            <th width="25%" class="pl-3" style="vertical-align: middle; background: whitesmoke">
                                Instansi</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $email->user->opd->nama }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Jenis Permohonan
                            </th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $email->jenis_permohonan }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Nama Email</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $email->nama_email }}@sukoharjokab.go.id" disabled></td>
                        </tr>
                        @if ($email->jenis_permohonan == 'Perubahan')
                            <tr>
                                <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Email Lama
                                </th>
                                <td><input class="form-control bg-transparent border-0" type="text"
                                        value="{{ $email->email_lama }}@sukoharjokab.go.id" disabled></td>
                            </tr>
                        @endif
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Nama Pengguna</th>
                            <td><input class="form-control bg-transparent border-0" type="text"
                                    value="{{ $email->nama_pengguna }}" disabled></td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Password</th>
                            <td>
                                <div class="input-group">
                                    <input type="password" id="password{{ $email->id }}"
                                        class="form-control bg-transparent border-0" name="password"
                                        value="{{ $email->password }}" disabled>
                                    <div class="input-group-append">
                                        <span id="mybutton{{ $email->id }}"
                                            onclick="showPassword2('password{{ $email->id }}', 'mybutton{{ $email->id }}')"
                                            class="input-group-text bg-transparent border-0">
                                            <i class="fas fa-eye"></i>
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Status</th>
                            <td class="pl-3"><span class="badge badge-{{ $warna }}">{{ $email->statusemail->status }}</span></td>
                        </tr>
                        @if ($email->status == 4)
                        <tr>
                            <th class="pl-3" style="vertical-align: middle; background: whitesmoke">Alasan/Keterangan</th>
                            <td class="pl-3"><p class="text-danger mt-3">{{ $email->keterangan }}</p></td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke">
                <button type="submit" class="btn btn-secondary btn-shadow" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
