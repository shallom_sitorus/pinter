<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email</title>
</head>

<body>
    <div>
        <table>
            <tbody>
                <tr>
                    <th align="right">Sukoharjo, {{ date('d F Y', strtotime($email->created_at)) }}</th>
                </tr>
                <tr>
                    <th width="10%">Nomor</th>
                    <td width="2%">:</td>
                    <td>{{ $email->no_email }}</td>
                </tr>
                <tr>
                    <th>Lampiran</th>
                    <td>:</td>
                    <td></td>
                </tr>
                <tr>
                    <th>Hal</th>
                    <td>:</td>
                    <td>Permohonan {{ $email->jenis_permohonan }} Email</td>
                </tr>
            </tbody>
        </table>
    </div>

    <p align="justify" style="text-indent: 20px">
        Untuk meningkatkan pelayanan Pemerintah kepada masyarakat dalam rangka meningkatkan e-government yang baik dan
        terarah melalui jaringan internet dan aplikasi berbasis teknologi digital, kami bermaksud mengajukan Permohonan
        {{ $email->jenis_permohonan }} email {{ $email->user->opd->nama }} sebagai media komunikasi dan informasi dengan
        masyarakat luas.
    </p>
    <p align="justify" style="text-indent: 20px;">
        Sehubungan dengan hal tersebut di atas, berikut kami sertakan data informasi sebagai berikut:
    </p>
    <div>
        <table border="1" cellpadding="4" width="100%">
            <tbody>
                <tr>
                    <th width="20%" style="background-color: rgb(177, 177, 177);">Email</th>
                    <td width="80%">{{ $email->nama_email }}</td>
                </tr>
                @if ($email->jenis_permohonan == 'Perubahan')
                    <tr>
                        <th style="background-color: rgb(177, 177, 177);">Email lama</th>
                        <td>{{ $email->email_lama }}</td>
                    </tr>
                @endif
                <tr>
                    <th style="background-color: rgb(177, 177, 177);">Password</th>
                    <td>{{ $email->password }}</td>
                </tr>
            </tbody>
        </table>
        <p align="justify" style="text-indent: 20px;">
            Demikian Permohonan ini disampaikan, atas perhatian dan kerjasamanya diucapkan terimakasih.
        </p>

        <table width="100%">
            <tbody>
                <tr>
                    <th width="60%"></th>
                    <th align="center" width="40%">Kepala {{ $email->user->opd->nama }} <br> Kabupaten Sukoharjo</th>
                </tr><br><br><br><br><br>
                <tr>
                    <th width="60%"></th>
                    <th align="center">(______________________________)</th>
                </tr>
                <tr>
                    <th width="60%"></th>
                    <th align="left">NIP. <span style="display: none">(___________________________)</span></th>
                </tr>

            </tbody>
        </table>
    </div>
</body>

</html>
