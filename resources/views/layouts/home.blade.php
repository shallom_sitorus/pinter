<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>PINTER | Pelayanan Admnistrasi Sistem Elektronik</title>

    <link rel="icon" type="image/png" href="{!! asset('img/kabsukoharjo.png') !!}">
    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css">
    <!-- Selectpicker -->
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{!! asset('css/owl.carousel.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/owl.theme.default.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/daterangepicker.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-tagsinput.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/select2.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/selectric.css') !!}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{!! asset('css/style.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/components.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/custom.css') !!}">
</head>

<body>
    <div id="app">
        <div class="main-wrapper">
            @include('partials.navbar')
            <div class="main-sidebar">
                @include('partials.sidebar')
            </div>

            <!-- Main Content -->
            <div class="main-content">
                @yield('content')
            </div>
            <footer class="main-footer">
                <div class="footer-left">
                    Dinas Komunikasi dan Informatika Kabupaten Sukoharjo
                    {{-- Dinas Komunikasi dan Informatika Kabupaten Sukoharjo <a href="https://nauval.in/">Muhamad Nauval Azhar</a> --}}
                </div>
                <div class="footer-right">
                    Copyright &copy; 2023
                </div>
            </footer>
        </div>
    </div>

    <!-- General JS Scripts -->
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chocolat/0.4.21/js/jquery.chocolat.min.js"></script>

    <!-- Search Box -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script>
        $(".selectpicker").selectpicker();
    </script>

    <script src="{!! asset('js/stisla.js') !!}"></script>

    <!-- JS Libraies -->
    <script src="{!! asset('js/owl.carousel.min.js') !!}"></script>
    <script src="{!! asset('js/cleave.min.js') !!}"></script>
    <script src="{!! asset('js/cleave-phone.id.js') !!}"></script>
    <script src="{!! asset('js/jquery.pwstrength.min.js') !!}"></script>
    <script src="{!! asset('js/daterangepicker.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-tagsinput.min.js') !!}"></script>
    <script src="{!! asset('js/select2.full.min.js') !!}"></script>
    <script src="{!! asset('js/jquery.selectric.min.js') !!}"></script>
    <script src="{!! asset('js/jquery.uploadPreview.min.js') !!}"></script>

    <!-- Template JS File -->
    <script src="{!! asset('js/scripts.js') !!}"></script>

    <!-- Page Specific JS File -->
    <script src="{!! asset('js/page/index.js') !!}"></script>
    <script src="{!! asset('js/page/features-post-create.js') !!}"></script>
    <script src="{!! asset('js/page/components-user.js') !!}"></script>
    <script src="{!! asset('js/page/forms-advanced-forms.js') !!}"></script>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

   
    <!-- SweetAlert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const href = $(this).attr("action");

            Swal.fire({
                title: "Apakah Anda yakin?",
                text: "Data yang dihapus tidak dapat kembali",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#808080",
                confirmButtonText: "Ya, hapus!",
                cancelButtonText: "Batal",
            }).then((result) => {
                if (result.value) {
                    document.location.href = href;
                }
            });
        });

        $(document).ready(function() {
            @if (Session::has('success'))
                Swal.fire({
                    title: 'Berhasil',
                    text: '{{ Session::get('success') }}',
                    icon: 'success',
                    timer: 3000,
                    timerProgressBar: true,
                    confirmButtonText: 'OK'
                });
            @endif

            @if (Session::has('successPermohonan'))
                Swal.fire({
                    title: 'Berhasil',
                    text: '{{ Session::get('successPermohonan') }}',
                    icon: 'success',
                    confirmButtonText: 'Lanjut'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                            'Perhatian',
                            'Silakan download formulir permohonan, beri tanda tangan, dan upload ke sistem.',
                            'info'
                        )
                    }
                });
            @endif

            @if (Session::has('successServer'))
                Swal.fire({
                    title: 'Berhasil',
                    text: '{{ Session::get('successServer') }}',
                    icon: 'success',
                    confirmButtonText: 'Lanjut'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                            'Perhatian',
                            'Silakan download formulir permohonan, beri tanda tangan, dan upload ke sistem saat status sudah berubah menjadi "Sedang Diproses".',
                            'info'
                        )
                    }
                });
            @endif

            @if (Session::has('error'))
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: 'error',
                    title: '{{ Session::get('error') }}'
                });
            @endif
        });
    </script>
    <!-- Datatables -->
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.min.js"></script>
    <script src="{!! asset('js/page/modules-datatables.js') !!}"></script>
    <script>
        // Menambahkan event listener untuk select
        document.getElementById("jenisakses").addEventListener("change", function() {
            // Mengecek apakah opsi yang dipilih adalah opsi "Lainnya"
            if (this.value === "lainnya") {
                // Menampilkan input teks
                document.getElementById("inputLainnya").style.display = "block";
            } else {
                // Menyembunyikan input teks
                document.getElementById("inputLainnya").style.display = "none";
            }
        });
    </script>
    <script src="{!! asset('js/custom.js') !!}"></script>
</body>

</html>
