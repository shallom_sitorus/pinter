<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ruangserver extends Model
{
    use HasFactory;
    protected $table = 'ruang_server';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function timpemohon()
    {
        return $this->hasMany(Pemohon::class, 'ruang_server_id');
    }

    public function statusserver(){
        return $this->belongsTo(Status::class, 'status');
    }
    
    public static function generateNoKartu()
    {
        $tahun = date('Y');
        $bulan = date('m');
        $tanggal = date('d');
        $lastNoKar = self::where('no_kartu_akses', 'like', '%'.$tahun.'%')->max('no_kartu_akses');
        if ($lastNoKar) {
            $lastTahun = substr($lastNoKar, 3, 4);
            if ($lastTahun == $tahun) {
                $noUrut = intval(substr($lastNoKar, -5)) + 1;
            } else {
                $noUrut = 1;
            }
        } else {
            $noUrut = 1;
        }
        $noKar = 'RS-'. $tahun . $bulan . $tanggal . sprintf('%05d', $noUrut);
        return $noKar;
    }
}
