<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Devweb extends Model
{
    use HasFactory;
    protected $table = 'dev_web';

    protected $guarded = ['id'];

    public function hosting()
    {
        return $this->belongsTo(Hosting::class, 'hosting');
    }
}
