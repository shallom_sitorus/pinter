<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use HasFactory;

    protected $table = 'email';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function statusemail(){
        return $this->belongsTo(Status::class, 'status');
    }

    public static function generateNomor()
    {
        $tahun = date('Y');
        $bulan = date('m');
        $tanggal = date('d');
        $lastNoKar = self::where('no_email', 'like', '%'.$tahun.'%')->max('no_email');
        if ($lastNoKar) {
            $lastTahun = substr($lastNoKar, 3, 4);
            if ($lastTahun == $tahun) {
                $noUrut = intval(substr($lastNoKar, -5)) + 1;
            } else {
                $noUrut = 1;
            }
        } else {
            $noUrut = 1;
        }
        $noKar = 'EM-'. $tahun . $bulan . $tanggal . sprintf('%05d', $noUrut);
        return $noKar;
    }
}
