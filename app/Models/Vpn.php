<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vpn extends Model
{
    use HasFactory;

    protected $table = 'vpn';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function statusvpn(){
        return $this->belongsTo(Status::class, 'status');
    }

    public static function generateNomor()
    {
        $tahun = date('Y');
        $bulan = date('m');
        $tanggal = date('d');
        $lastNoKar = self::where('no_vpn', 'like', '%'.$tahun.'%')->max('no_vpn');
        if ($lastNoKar) {
            $lastTahun = substr($lastNoKar, 3, 4);
            if ($lastTahun == $tahun) {
                $noUrut = intval(substr($lastNoKar, -5)) + 1;
            } else {
                $noUrut = 1;
            }
        } else {
            $noUrut = 1;
        }
        $noKar = 'VP-'. $tahun . $bulan . $tanggal . sprintf('%05d', $noUrut);
        return $noKar;
    }
}
