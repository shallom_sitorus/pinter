<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\Status;
use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EmailController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tahun) {
            if (Auth::user()->role_id == 2) {
                $email = Email::where('user_id', Auth::user()->id)->where(DB::raw('YEAR(created_at)'), $request->tahun)->latest()->get();
            } else {
                $email = Email::where(DB::raw('YEAR(created_at)'), $request->tahun)->latest()->get();
            }
        } else {
            if (Auth::user()->role_id == 2) {
                $email = Email::where('user_id', Auth::user()->id)->where(DB::raw('YEAR(created_at)'), now())->latest()->get();
            } else {
                $email = Email::where(DB::raw('YEAR(created_at)'), now())->latest()->get();
            }
        }
        
        $tahun = DB::table('email')->select(DB::raw('YEAR(created_at) as tahun'))->orderBy(DB::raw('YEAR(created_at)'), 'desc')->groupBy(DB::raw('YEAR(created_at)'))->pluck('tahun');
        
        $data = [
            'emails' => $email,
            'tahun' => $tahun,
            'jenispermohon' => Email::select('jenis_permohonan')->groupBy('jenis_permohonan')->pluck('jenis_permohonan'),
            'status' => Status::all()
        ];
        return view('email.email', $data);
    }

    public function baru()
    {
        $no_email = Email::generateNomor();
        return view('email.baru-email', [
            'no_email' => $no_email
        ]);
    }

    public function createBaru(Request $request)
    {        
        $validator = Validator::make($request->all(), [
            'nama_email' => 'required|unique:email',
            'nama_pengguna' => 'required|string',
            'password' => 'required|string|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@&]).*$/',
        ]);

        if ($validator->fails()) {
            return redirect('/email/baru')->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        Email::create([
            'user_id' => $request->user_id,
            'jenis_permohonan' => $request->jenis_permohonan,
            'nama_email' => $request->nama_email,
            'password' => $request->password,
            'nama_pengguna' => $request->nama_pengguna,
            'status' => $request->status,
            'no_email' => $request->no_email,
        ]);

        return redirect('/email')->with('successPermohonan', 'Permohonan email baru berhasil dibuat!');
    }

    public function edit($id)
    {
        return view('email.edit-email', [
            'email' => Email::find($id)
        ]);
    }

    public function update(Request $request, $id)
    {
        $email = Email::find($id);

        $rules = [
            'password' => 'required|string|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@&]).*$/',
        ];

        if($request->nama_email != $email->nama_email) {
            $rules['nama_email'] = 'required|unique:email';
        }
        if($request->nama_pengguna){
            $rules['nama_pengguna'] = 'required|string';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/email/edit/'. $email->id)->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($email->jenis_permohonan == 'Baru') {
            $email->update([
                'nama_email' => $request->nama_email,
                'password' => $request->password,
                'nama_pengguna' => $request->nama_pengguna,
            ]);
        } elseif ($email->jenis_permohonan == 'Perubahan') {
            $email->update([
                'nama_email' => $request->nama_email,
                'password' => $request->password,
            ]);
        } else {
            $email->update([
                'password' => $request->password,
            ]);
        }

        return redirect('/email')->with('success', 'Permohonan email berhasil diubah!');
    }

    public function perubahan($id)
    {
        $no_email = Email::generateNomor();
        return view('email.perubahan-email', [
            'email' => Email::find($id),
            'no_email' => $no_email
        ]);
    }

    public function createPerubahan(Request $request, $id)
    {
        $email = Email::find($id);

        $rules = [
            'nama_email' => 'required|unique:email',
        ];

        if ($request->password) {
            $rules['password'] = 'string|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@&]).*$/';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/email/perubahan/'. $email->id)->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->password) {
            $pass = $request->password;
        } else {
            $pass = $request->passLama;
        }
        
        $email->update([
            'is_changed' => 1,
        ]);

        Email::create([
            'user_id' => $request->user_id,
            'jenis_permohonan' => $request->jenis_permohonan,
            'nama_email' => $request->nama_email,
            'email_lama' => $request->email_lama,
            'password' => $pass,
            'nama_pengguna' => $request->nama_pengguna,
            'status' => $request->status,
            'old_email_id' => $email->id,
            'no_email' => $request->no_email,
        ]);

        return redirect('/email')->with('successPermohonan', 'Permohonan perubahan email berhasil dibuat!');
    }

    public function gantipass($id)
    {
        $no_email = Email::generateNomor();
        return view('email.gantipass-email', [
            'email' => Email::find($id),
            'no_email' => $no_email
        ]);
    }

    public function createGantipass(Request $request, $id)
    {
        $email = Email::find($id);

        $rules = [
            'password' => 'required|string|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@&]).*$/',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/email/gantipass/'. $email->id)->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        $email->update([
            'is_changed' => 1,
        ]);

        Email::create([
            'user_id' => $request->user_id,
            'jenis_permohonan' => $request->jenis_permohonan,
            'nama_email' => $request->nama_email,
            'password' => $request->password,
            'nama_pengguna' => $request->nama_pengguna,
            'status' => $request->status,
            'old_email_id' => $email->id,
            'no_email' => $request->no_email,
        ]);

        return redirect('/email')->with('successPermohonan', 'Permohonan ganti password email berhasil dibuat!');
    }

    public function uploadFormulir(Request $request, $id)
    {   
        $email = Email::find($id);

        $validator = Validator::make($request->all(), [
            'surat' => 'required|file|mimes:pdf|max:5120'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('surat')) {
            $nama_file = date('Ymdhis') . '_' . $request->file('surat')->getClientOriginalName();
            $request->file('surat')->move('form_email/', $nama_file);
            if ($request->oldFile) {
                unlink('form_email/'. $request->oldFile);
            }
        } else {
            $nama_file = $request->oldFile;
        }

        $email->update([
            'surat' => $nama_file,
        ]);

        return redirect('/email')->with('success', 'Formulir permohonan berhasil diupload!');
    }

    public function status(Request $request, $id)
    {
        $email = Email::find($id);

        if ($request->status == 4) {
            Email::where('id', $email->old_email_id)->update([
                'is_changed' => null
            ]);
        }
        $email->update([
            'status' => $request->status,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('/email')->with('success', 'Status berhasil diubah!');
    }

    public function delete($id)
    {
        $delete = Email::find($id);
        $delete->delete();
        return redirect('/email')->with('success', 'Permohonan email berhasil dihapus!');
    }

    public function cetak($id)
    {
        $filename = "email.pdf";
        $data = [
            'title' => 'Formulir Permohonan Email',
            'email' => Email::find($id),
            'jenispermohon' => Email::select('jenis_permohonan')->groupBy('jenis_permohonan')->pluck('jenis_permohonan')
        ];
        $html = view('email.cetak', $data);

        $pdf = new TCPDF;

        $pdf::SetTitle('Email');
        $pdf::SetHeaderMargin(30); // set margin untuk header
        $pdf::AddPage('P', 'F4');
        $pdf::Image('img/kabsukoharjo.jpeg', 10, 10, 25);
        $pdf::SetFont('times', 'B', 14);
        $pdf::Cell(0, 10, $data['email']['user']['opd']['nama']. ' Kabupaten Sukoharjo', 0, 1, 'C');
        $pdf::SetFont('times', '', 12);
        $pdf::Cell(0, 10, $data['email']['user']['opd']['alamat'], 0, 1, 'C');
        $pdf::Cell(0, 10, 'Telp. '.$data['email']['user']['opd']['telp'].', email: '. $data['email']['user']['opd']['email'], 0, 1, 'C');
        $pdf::Ln(10); // jarak antara header dan konten
        $pdf::SetLineWidth(0.1); // Membuat garis bawah pada header
        $pdf::Line(10, 45, 200, 45);
        $pdf::SetMargins(15, 25, 25);
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output($filename, 'I');
    }

}
