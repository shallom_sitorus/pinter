<?php

namespace App\Http\Controllers;

use App\Models\Opd;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function add($id){
        return view('user.add-user', [
            'opd' => Opd::find($id),
            'roles' => Role::all()
        ]);
    }

    public function create(Request $request)
    {        
        $validator = Validator::make($request->all(), [
            'nip' => 'required|min:3|unique:users',
            'nama' => 'required|max:255',
            'email' => 'email:dns|unique:users',
            'password' => 'required|min:8|max:255', 
            'telp' => 'required',
            'jabatan' => 'required|max:255',
            'image' => 'image|file|mimes:jpeg,png,jpg,gif,svg|max:1024'
        ]);

        if ($validator->fails()) {
            return redirect('opd/'. $request->opd_kode .'/user/add')->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('image')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('img/fotousers/', $nama_gambar);
        } else {
            $nama_gambar = '';
        }

        User::create([
            'nip' => $request->nip,
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'telp' => $request->telp,
            'jabatan' => $request->jabatan,
            'role_id' => $request->role_id,
            'opd_kode' => $request->opd_kode,
            'foto' => $nama_gambar
        ]);

        return redirect('/opd/detail/'. $request->opd_kode)->with('success', 'User berhasil ditambahkan!');
    }

    public function edit(Opd $opd, User $user){
        return view('user.edit-user', [
            'opd' => $opd,
            'user' => $user,
            'roles' => Role::all()
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $rules = [
            'nama' => 'required|max:255',
            'password' => 'required|min:8|max:255',
            'telp' => 'required',
            'jabatan' => 'required|max:255',
        ];

        if($request->email != $user->email) {
            $rules['email'] = 'email:dns|unique:users';
        }
        if($request->nip != $user->nip) {
            $rules['nip'] = 'required|min:3|unique:users';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/opd/'. $request->opd_kode .'/user/edit/'. $user->id)->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        // Pengkondisian password
        if($request->password != $request->password_lama) {
            $passwordHash = Hash::make($request->password);
        } else {
            $passwordHash = $request->password_lama;
        }

        // Pengkondisian upload gambar
        if ($request->file('image')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('img/fotousers/', $nama_gambar);
            if ($request->oldImage) {
                unlink('img/fotousers/'. $request->oldImage);
            }
        } else {
            $nama_gambar = $request->oldImage;
        }
        
        $user->update([
            'nip' => $request->nip,
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => $passwordHash,
            'telp' => $request->telp,
            'jabatan' => $request->jabatan,
            'role_id' => $request->role_id,
            'opd_kode' => $request->opd_kode,
            'foto' => $nama_gambar
        ]);

        return redirect('/opd/detail/'. $request->opd_kode)->with('success', 'User berhasil diubah!');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/opd/detail/'. $user->opd->id)->with('success', 'User berhasil dihapus!');
    }

    public function profile()
    {
        $data = [
            'user' => User::find(Auth::user()->id)
        ];
        
        return view('user.profile-user', $data);
    }

    public function updateProfile(Request $request, $id)
    {
        $user = User::find(Auth::user()->id);

        $rules = [
            'nama' => 'required|max:255',
            'telp' => 'required',
            'jabatan' => 'required|max:255',
        ];

        if($request->email != $user->email) {
            $rules['email'] = 'email:dns|unique:users';
        }
        if($request->nip != $user->nip) {
            $rules['nip'] = 'required|min:3|unique:users';
        }
        if ($request->password) {
            $rules['password'] = 'min:8|max:255';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/profile')->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        // Pengkondisian password
        if($request->password != $request->password_lama) {
            $passwordHash = Hash::make($request->password);
        } else {
            $passwordHash = $request->password_lama;
        }

        // Pengkondisian upload gambar
        if ($request->file('image')) {
            $nama_gambar = date('Ymdhis') . '_' . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('img/fotousers/', $nama_gambar);
            if ($request->oldImage) {
                unlink('img/fotousers/'. $request->oldImage);
            }
        } else {
            $nama_gambar = $request->oldImage;
        }
        
        $user->update([
            'nip' => $request->nip,
            'nama' => $request->nama,
            'email' => $request->email,
            'password' => $passwordHash,
            'telp' => $request->telp,
            'jabatan' => $request->jabatan,
            'foto' => $nama_gambar
        ]);

        return redirect('/profile')->with('success', 'Profile berhasil diupdate!');
    }
}
