<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Email;
use App\Models\Hakakses;
use App\Models\Hosting;
use App\Models\Ruangserver;
use App\Models\Status;
use App\Models\Vpn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $user = User::where('opd_kode', Auth::user()->opd_kode)->get();
        $user_opd = [];
        foreach ($user as $u) {
            $user_opd[] = [
                $u->id
            ];
        }
        
        if (Auth::user()->role_id == 1) {
            $hosting = Hosting::where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $h_proses = Hosting::where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $h_selesai = Hosting::where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $h_ditolak = Hosting::where('status', 4)->where(DB::raw('YEAR(created_at)'), now())->count();
            $h_total = Hosting::where(DB::raw('YEAR(created_at)'), now())->count();
            $email = Email::where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $e_proses = Email::where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $e_selesai = Email::where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $e_ditolak = Email::where('status', 4)->where(DB::raw('YEAR(created_at)'), now())->count();
            $e_total = Email::where(DB::raw('YEAR(created_at)'), now())->count();
            $vpn = Vpn::where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $v_proses = Vpn::where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $v_selesai = Vpn::where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $v_ditolak = Vpn::where('status', 4)->where(DB::raw('YEAR(created_at)'), now())->count();
            $v_total = Vpn::where(DB::raw('YEAR(created_at)'), now())->count();
            $ruang = Ruangserver::where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $r_proses = Ruangserver::where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $r_selesai = Ruangserver::where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $r_total = Ruangserver::where(DB::raw('YEAR(created_at)'), now())->count();
            $hakakses = Hakakses::where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $k_proses = Hakakses::where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $k_selesai = Hakakses::where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $k_total = Hakakses::where(DB::raw('YEAR(created_at)'), now())->count();
        } else {
            $hosting = Hosting::where('user_id', $user_opd)->where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $h_proses = Hosting::where('user_id', $user_opd)->where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $h_selesai = Hosting::where('user_id', $user_opd)->where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $h_ditolak = Hosting::where('user_id', $user_opd)->where('status', 4)->where(DB::raw('YEAR(created_at)'), now())->count();
            $h_total = Hosting::where('user_id', $user_opd)->where(DB::raw('YEAR(created_at)'), now())->count();
            $email = Email::where('user_id', $user_opd)->where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $e_proses = Email::where('user_id', $user_opd)->where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $e_selesai = Email::where('user_id', $user_opd)->where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $e_ditolak = Email::where('user_id', $user_opd)->where('status', 4)->where(DB::raw('YEAR(created_at)'), now())->count();
            $e_total = Email::where('user_id', $user_opd)->where(DB::raw('YEAR(created_at)'), now())->count();
            $vpn = Vpn::where('user_id', $user_opd)->where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $v_proses = Vpn::where('user_id', $user_opd)->where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $v_selesai = Vpn::where('user_id', $user_opd)->where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $v_ditolak = Vpn::where('user_id', $user_opd)->where('status', 4)->where(DB::raw('YEAR(created_at)'), now())->count();
            $v_total = Vpn::where('user_id', $user_opd)->where(DB::raw('YEAR(created_at)'), now())->count();
            $ruang = Ruangserver::where('user_id', $user_opd)->where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $r_proses = Ruangserver::where('user_id', $user_opd)->where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $r_selesai = Ruangserver::where('user_id', $user_opd)->where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $r_total = Ruangserver::where('user_id', $user_opd)->where(DB::raw('YEAR(created_at)'), now())->count();
            $hakakses = Hakakses::where('user_id', $user_opd)->where('status', 1)->where(DB::raw('YEAR(created_at)'), now())->count();
            $k_proses = Hakakses::where('user_id', $user_opd)->where('status', 2)->where(DB::raw('YEAR(created_at)'), now())->count();
            $k_selesai = Hakakses::where('user_id', $user_opd)->where('status', 3)->where(DB::raw('YEAR(created_at)'), now())->count();
            $k_total = Hakakses::where('user_id', $user_opd)->where(DB::raw('YEAR(created_at)'), now())->count();
        }

        $data = [
            'hosting' => $hosting,
            'h_proses' => $h_proses,
            'h_selesai' => $h_selesai,
            'h_ditolak' => $h_ditolak,
            'h_total' => $h_total,
            'email' => $email,
            'e_proses' => $e_proses,
            'e_selesai' => $e_selesai,
            'e_ditolak' => $e_ditolak,
            'e_total' => $e_total,
            'vpn' => $vpn,
            'v_proses' => $v_proses,
            'v_selesai' => $v_selesai,
            'v_ditolak' => $v_ditolak,
            'v_total' => $v_total,
            'ruang' => $ruang,
            'r_proses' => $r_proses,
            'r_selesai' => $r_selesai,
            'r_total' => $r_total,
            'hakakses' => $hakakses,
            'k_proses' => $k_proses,
            'k_selesai' => $k_selesai,
            'k_total' => $k_total,
            'tahun' => date('Y'),
            'status' => Status::all(),
        ];
        return view('dashboard', $data);
    }
}
