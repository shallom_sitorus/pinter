/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";

// Filter Tahun
const form = document.getElementById("myForm");
const select = document.getElementById("mySelect");
select.addEventListener("change", function () {
    form.submit();
});

// Filter Layanan
$(document).ready(function() {
    var table = $('#layananTable').DataTable({
        columnDefs: [{
            targets: 4,
            data: 'jenis_permohonan'
        }, {
            targets: 6,
            data: 'status'
        }, ],
    });

    $('#jenis_filter, #status_filter').on('change', function() {
        table.column(4).search($('#jenis_filter').val()).column(6).search($('#status_filter').val())
            .draw();
    });
});

// Filter Layanan v2
$(document).ready(function() {
    var table = $('#layananTable-2').DataTable({
        columnDefs: [{
            targets: 3,
            data: 'jenis_permohonan'
        }, {
            targets: 6,
            data: 'status'
        }, ],
    });

    $('#jenis_filter, #status_filter').on('change', function() {
        table.column(3).search($('#jenis_filter').val()).column(6).search($('#status_filter').val())
            .draw();
    });
});

// Filter Layanan Hosting
$(document).ready(function() {
    var table = $('#hostingTable').DataTable({
        columnDefs: [{
            targets: 3,
            data: 'jenis_permohonan'
        }, {
            targets: 5,
            data: 'status'
        }, ],
    });

    $('#jenis_filter, #status_filter').on('change', function() {
        table.column(3).search($('#jenis_filter').val()).column(5).search($('#status_filter').val())
            .draw();
    });
});

// Filter Layanan Server
$(document).ready(function() {
    var table = $('#serverTable').DataTable({
        columnDefs: [{
            targets: 6,
            data: 'status'
        }, ],
    });

    $('#status_filter').on('change', function() {
        table.column(6).search($('#status_filter').val()).draw();
    });
});

// Filter Hak Akses
$(document).ready(function() {
    var table = $('#tableAkses').DataTable({
        columnDefs: [{
            targets: 2,
            data: 'jenis_hak_akses'
        }, {
            targets: 6,
            data: 'status'
        }, ],
    });

    $('#jenis_filter, #status_filter').on('change', function() {
        table.column(2).search($('#jenis_filter').val()).column(6).search($('#status_filter').val())
            .draw();
    });
});

// Filter Hak Akses 2
$(document).ready(function() {
    var table = $('#tableAkses-2').DataTable({
        columnDefs: [{
            targets: 1,
            data: 'jenis_hak_akses'
        }, {
            targets: 6,
            data: 'status'
        }, ],
    });

    $('#jenis_filter, #status_filter').on('change', function() {
        table.column(1).search($('#jenis_filter').val()).column(6).search($('#status_filter').val())
            .draw();
    });
});

/* Mengoptimalkan fungsi choose file */
$(document).ready(function() {
    var fileInput = document.getElementsByClassName("custom-file-input");

    for (var i = 0; i < fileInput.length; i++) {
        var input = fileInput[i];
        input.addEventListener("change", function(e) {
            var files = [];
            for (var i = 0; i < $(this)[0].files.length; i++) {
                files.push($(this)[0].files[i].name);
            }
            $(this).next(".custom-file-label").html(files.join(", "));
        });
    }
});

function showPassword() {
    var x = document.getElementById('password').type;

    if (x == 'password') {
        document.getElementById('password').type = 'text';
        document.getElementById('mybutton').innerHTML = `<i class="fas fa-eye-slash"></i>`;
        
    } else {
        document.getElementById('password').type = 'password';
        document.getElementById('mybutton').innerHTML = `<i class="fas fa-eye"></i>`;
    }
}

function showPassword2(inputId, buttonId) {
    var x = document.getElementById(inputId).type;

    if (x == 'password') {
        document.getElementById(inputId).type = 'text';
        document.getElementById(buttonId).innerHTML = `<i class="fas fa-eye-slash"></i>`;

    } else {
        document.getElementById(inputId).type = 'password';
        document.getElementById(buttonId).innerHTML = `<i class="fas fa-eye"></i>`;
    }
}