<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vpn', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->cascadeOnUpdate()->nullOnDelete();
            $table->text('keperluan');
            $table->date('tgl_awal');
            $table->date('tgl_berakhir');
            $table->string('password');
            $table->text('permasalahan')->nullable();
            $table->string('ip_vpn')->nullable();
            $table->string('jenis_permohonan');
            $table->string('no_vpn')->nullable();
            $table->foreignId('status')->nullable()->constrained('status')->cascadeOnUpdate()->nullOnDelete();
            $table->text('keterangan')->nullable();
            $table->string('surat')->nullable();
            $table->integer('is_changed')->nullable();
            $table->integer('old_vpn_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vpn');
    }
};
