<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hak_akses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('jenis_hak_akses')->nullable()->constrained('jenis_hak_akses')->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('pihak_ketiga')->nullable()->constrained('pihak_ketiga')->cascadeOnUpdate()->nullOnDelete();
            $table->text('alasan');
            $table->date('tanggal_awal');
            $table->date('tanggal_akhir');
            $table->string('waktu')->nullable();
            $table->string('no_hak_akses')->nullable();
            $table->foreignId('status')->nullable()->constrained('status')->cascadeOnUpdate()->nullOnDelete();
            $table->text('keterangan')->nullable();
            $table->string('surat')->nullable();
            $table->integer('is_changed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hak_akses');
    }
};
