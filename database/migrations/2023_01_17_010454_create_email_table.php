<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->cascadeOnUpdate()->nullOnDelete();
            $table->string('nama_email');
            $table->string('password');
            $table->string('nama_pengguna');
            $table->string('email_lama')->nullable();
            $table->string('jenis_permohonan');
            $table->string('no_email')->nullable();
            $table->foreignId('status')->nullable()->constrained('status')->cascadeOnUpdate()->nullOnDelete();
            $table->text('keterangan')->nullable();
            $table->string('surat')->nullable();
            $table->integer('is_changed')->nullable();
            $table->integer('old_email_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email');
    }
};
