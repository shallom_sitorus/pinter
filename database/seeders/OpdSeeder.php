<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class OpdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('opd')->insert([[
            'nama' => 'Dinas Komunikasi dan Informatika',
            'email' => 'diskominfo@sukoharjokab.go.id',
            'telp' => '0271593068',
            'alamat' => 'Jl. Jenderal Sudirman Nomor 199 Sukoharjo'
        ], [
            'nama' => 'Dinas Pangan',
            'email' => 'dinaspangano@sukoharjokab.go.id',
            'telp' => '0271598765',
            'alamat' => 'Jl. Jenderal Sudirman Nomor 199 Sukoharjo'
        ], [
            'nama' => 'Dinas Sosial',
            'email' => 'dinsos@sukoharjokab.go.id',
            'telp' => '(0271) 593024',
            'alamat' => 'Jl. Veteran No.61, Kutorejo, Jetis, Kec. Sukoharjo, Kabupaten Sukoharjo, Jawa Tengah 57511'
        ], [
            'nama' => 'Dinas Lingkungan Hidup',
            'email' => 'blh@sukoharjokab.go.id',
            'telp' => '(0271) 591613',
            'alamat' => 'Jl. Tentara Pelajar, Jombor, Bendosari, Sukoharjo'
        ], [
            'nama' => 'Badan Keuangan Daerah',
            'email' => 'dppkad.sukoharjo@gmail.com',
            'telp' => '(0271) 591678',
            'alamat' => 'Gedung Soewarno Honggopati Jalan Jenderal Sudirman Nomor 199 Sukoharjo'
        ]]);
    }
}
