<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class VpnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vpn')->insert([[
            'user_id' => '2',
            'keperluan' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Neque accusamus asperiores eius officia error ad soluta temporibus.',
            'tgl_awal' => '2023-03-24',
            'tgl_berakhir' => '2023-03-31',
            'password' => 'P4ssword#1',
            'jenis_permohonan' => 'Baru',
            'no_vpn' => 'VP-2022040500001',
            'status' => '1',
            'is_changed' => '0',
            'created_at' => '2023-02-28 12:13:00'
        ], [
            'user_id' => '2',
            'keperluan' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Neque accusamus asperiores eius officia error ad soluta temporibus.',
            'tgl_awal' => '2022-03-24',
            'tgl_berakhir' => '2022-03-31',
            'password' => 'P4ssword#2',
            'jenis_permohonan' => 'Baru',
            'no_vpn' => 'VP-2022041000002',
            'status' => '1',
            'is_changed' => '0',
            'created_at' => '2023-01-28 12:13:00'
        ]]);
    }
}
