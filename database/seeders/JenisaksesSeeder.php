<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JenisaksesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_hak_akses')->insert([[
            'nama' => 'Collocation aplikasi baru',
        ], [
            'nama' => 'Update aplikasi yang sudah ada',
        ]]);
    }
}
