<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class HakaksesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hak_akses')->insert([[
            'user_id' => '2',
            'jenis_hak_akses' => '1',
            'pihak_ketiga' => '1',
            'alasan' => 'Untuk melihat informasi sebagai bahan keperluan tugas',
            'tanggal_awal' => '2023-03-08',
            'tanggal_akhir' => '2023-03-12',
            'waktu' => '2 bulan',
            'no_hak_akses' => 'HA-2023040500001',
            'status' => '1',
            'created_at' => '2023-02-28 12:13:00'
        ]]);
    }
}
