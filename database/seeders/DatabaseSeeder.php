<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            OpdSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            PihakketigaSeeder::class,
            JenisaksesSeeder::class,
            StatusSeeder::class,
            RuangserverSeeder::class,
            HakaksesSeeder::class,
            LayananhostingSeeder::class,
            EmailSeeder::class,
            VpnSeeder::class,
            DevwebSeeder::class
        ]);

        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
